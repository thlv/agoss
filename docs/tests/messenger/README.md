# Instant-Messaging Dienst

Um einen intensiven Informationsaustausch auf unterschiedlichen Ebenen zu unterstützen, soll ein einheitlicher, zentral betriebener und sicherer Instant-Messenger getestet werden.

![Screenshot Bundesmessenger](files/bum-screenshot.png)

## Gründe für den Test des Bundesmessenger

* Für den Test wurde der Bundesmessenger (BuM) ausgewählt, denn
    - zahlreiche [Voraussetzungen](https://gitlab.opencode.de/bwi/bundesmessenger/info/-/blob/main/warum-bum.md) für den Betrieb eines Messenger Dienstes in der Öffentlichen Verwaltung sind dort bereits umgesetzt.
    - als Server Komponente kommt die [Synapse](https://github.com/matrix-org/synapse) Implementierung des [Matrix Protokolls](https://de.wikipedia.org/wiki/Matrix_(Kommunikationsprotokoll)) zum Einsatz.
    - auf der Client Seite wird ein [Fork](https://de.wikipedia.org/wiki/Abspaltung_(Softwareentwicklung)) von [Element.io](https://element.io/) verwendet.
    - die Serverkomponenten sind [cloud-native](https://de.wikipedia.org/wiki/Cloud-native_Computing)
* alle Komponenten, auch zum [Deployment im Kubernetes Umfeld](https://gitlab.opencode.de/bwi/bundesmessenger/backend) wurden von der [BWI](https://social.bund.de/@bwi) auf der [OpenCode Plattform](https://gitlab.opencode.de/bwi/bundesmessenger) zur Nachnutzung veröffentlicht.

## grundsätzliches
* Der Messenger steht im Rahmen des Tests als optional nutzbarer zentraler Dienst zur Verfügung.
* Der Testbetrieb erfolgt beim IT-Landesdienstleister.
* Zur Authentifizierung der Tester:innen wurde der Messenger an das zentrale IAM angebunden.
    - Dadurch entfällt die Pflege einer separaten Benutzerdatenbank und
    - Testaccounts können einfach per Zugehörigkeit zu einer entsprechenden Benutzergruppe hinzugefügt werden.
* Im Rahmen des Tests ist keine Föderation mit anderen Instanzen von Matrix bzw. Bundesmessenger z.B. bei anderen Bundes/Landesbehörden eingesetzt sind.
* Verfügbarkeit und Service sind während der Testphase eingeschränkt.

## Häufig gestellte Fragen

Antworten auf häufig gestellte Fragen zum Bundesmessenger werden im [Bereich FAQ](./faq.md)  gesammelt.
