# Häufig gestellte Fragen

## Gibt es einen Windows-Client für den Bundesmessenger?
Bisher gibt es keine native Windows-Anwendung für den Bundesmessenger.
Eine Variante ist der [Progressive Web App (PWA)](https://de.wikipedia.org/wiki/Progressive_Web_App) Modus von [Chrome](https://de.wikipedia.org/wiki/Google_Chrome) basierten Browsern. Dadurch kann der Messenger wie andere Anwendungen im Startmenü oder in der Taskleiste angeheftet werden.
Nachdem die Weboberfläche des Bundesmessengers geöffnet ist, wird über die "App Funktion" des Browsers (Tastenkombination ++alt+f++ oder im Menü unter _Einstellungen -> Apps_) aus der Website eine "Windows App" erzeugt.
