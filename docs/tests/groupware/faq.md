# häufig gestellte Fragen

## Integration von E-Mails in VIS
E-Mails aus Thunderbird können per **Drag-and-drop** im VIS-Smartclient abgelegt werden. Dadurch entsteht im VIS-Dokument eine Datei vom Typ [.eml](https://en.wikipedia.org/wiki/Email#Filename_extensions)
Anhänge werden beim Import automatisch extrahiert und separat ins VIS-Dokument abgelegt.
