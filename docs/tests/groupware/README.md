# E-Mail, Kalender und Kontakte

Im Arbeitsalltag der Nutzer:innen spielt eine Groupware mit den Basiskomponenten E-Mail, Kalender und Kontakte eine zentrale Rolle.
Bei bestehenden proprietären Lösungen sind Client Anwendungen stark mit der darunter liegenden Server Infrastruktur verbandelt. Aus diesem Grund wird der Test auf die Server Ebene ausgeweitet und der Mailstack selbst mit beleuchtet.

## Anforderungen

* zentraler Betrieb in der Umgebung des IT-Landesdienstleisters
* Anbindung der Identitäten der Testteilnehmer:innen an bestehendes Identity & Access Management (IAM)
* Zugriff auf ein globales Adressbuch, welches sich in einer proprietären Umgebung befindet
* free/busy Ansicht von Benutzer:innen zwischen quelloffener Testumgebung und proprietärer Produktivumgebung
* E-Mailadressen im offiziellen Namensraum der Landesverwaltung
* Anbindung an e-Akte
    * Ablage in der elektronischen Akte, z.B.
        * per Schaltfläche in der Benutzeroberfläche von openDesk oder
        * via Drag & Drop
    * E-Mail verfassen, bzw. bearbeiten aus e-Akte System heraus
* Anbindung von Thunderbird als Fat Client z.B. über
    * [IMAP](https://de.wikipedia.org/wiki/Internet_Message_Access_Protocol)/[SMTP](https://de.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol) für E-Mail,
    * [CalDAV](https://de.wikipedia.org/wiki/CalDAV) für Kalender,
    * [CardDAV](https://de.wikipedia.org/wiki/CardDAV) für Adressbücher/Kontakte

## Vorstufe Test Thunderbird

* Für den Einsatz einer alternativen E-Mail Lösung ist entscheidend, das E-Mails in die elektonische Akte übernommen werden können. Diese Möglichkeit ist mit Thunderbird per Drag-and-Drop gegeben.
* Anbindung von Thunderbird an das bestehende E-Mailsystem erfolgt direkt über die IMAP- und SMTP Schnittstellen
* Mit dem Addon [TbSybnc](https://addons.thunderbird.net/en-us/thunderbird/addon/tbsync/) können darüber hinaus Kalender, Kontakte und Aufgaben des bestehenden Mailstack bezogen und in Thunderbird weiter verarbeitet werden.

![Screenshot Thunderbird](files/tb-screenshot.png)

## Testszenario openDesk Stufe 1

![Screenshot der openDesk E-Mailkomponente](files/od-screenshot.png)

* Aktuell laufen Tests mit Teilnehmer:innen aus verschiedenen Ressorts
* die Testumgebung ist derzeit autark von der bestehenden Umgebung, produktive Mailboxen sind also nicht angebunden
* Benutzer werden mithilfe eines [Import Skripts](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/-/releases/) ins Demo-System importiert

## Testszenario Stufe 2

* openDesk Instanz in der Umgebung des Landesdienstleisters, um Testfälle des [Anforderungsprofils](#anforderungen) bearbeiten zu können

## weiterführende Informationen

* Anleitungen für Benutzer und Administrator sind auf der offiziellen [Dokumentationsseite von openDesk](https://docs.opendesk.eu) verfügbar.
