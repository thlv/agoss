# LanguageTool erweiterte Rechtschreib- und Grammatikprüfung

Während des Tests wird auf die integrierte Rechtschreib- und Grammatikprüfung von LibreOffice verzichtet. Stattdessen wurde eine zentrale Testinstanz der Software [LanguageTool (LT)](https://languagetool.org/de) eingerichtet. Diese bietet z.B. Plausibilitätsprüfungen bei Datumsangaben, sowie erweiterte Grammatikprüfung auf Basis von [N-Gramm](https://de.wikipedia.org/wiki/N-Gramm).
Über die offene [API](https://languagetool.org/http-api/#/default) können beliebige Anwendungen zur Rechtschreib- und Grammatikprüfung an LT angebunden werden. Auf diese Weise wurde LT direkt in LibreOffice integriert.

![LanguageTool Screenshot](files/lt-screenshot.png)

## technische Hinweise

Eine zentrale Wortliste, die z.B. gängigen Abkürzungen und Begriffe enthält, kann über die Datei ``spelling_custom.txt`` in die Instanz eingebunden werden. Diese Datei liegt im Verzeichnis ``org/languagetool/resource/de/hunspell/spelling_custom.txt`` im LanguageTool Verzeichnis. Siehe hierzu Beitrag im [LanguageTool Forum](https://forum.languagetool.org/t/zentrale-wortlisten-zur-laufzeit/9291).
Für die Testinstanz wird die Liste in einem [eigenen Repository](https://gitlab.opencode.de/thlv/wortliste-languagetool) gepflegt.
