# Test Open-Source Office-Software in der Thüringer Landesverwaltung

Die Arbeitsgemeinschaft Open-Source-Software (AGOSS) innerhalb der Thüringer Landesverwaltung prüft den Einsatz verschiedener Open-Source basierter Office-Software auf geeigneten Arbeitsplatz PCs.

Siehe dazu [Präsentation des Kurzvortrags](https://www.bitkom.org/sites/main/files/2023-12/BFOSS23Praesentation-Schieke-Arbeitsgemeinschaft-Community-Innner-Source.pdf) der AGOSS vom September 2023 auf dem [Bitkom Forum Open Source](https://www.bitkom.org/bfoss23) in Erfurt.

## Ziele, Umfang und Ablauf

Eine Reihe quelloffener Software-Komponenten werden in der für E-Government zuständigen Abteilung des Thüringer Ministeriums für Digitales und Infrastruktur sowie einem Kernteam aus verschiedenen Ressorts getestet. Wichtig ist dabei, dass die Lösungen im produktiven Arbeitsumfeld genutzt werden, nicht in einer abgeschlossenen Testumgebung. Insbesondere kann dabei das teilweise komplexe Zusammenspiel mit Fachanwendungen untersucht werden, die in vielen Fällen auf proprietäre Software an den Schnittstellen setzt.

### gemeinsame Ziele

* Nachweisen grundsätzlicher technischer Machbarkeit
* Identifizieren möglicher Stolpersteine in technischer, organisatorischer, finanzieller und rechtlicher Dimension
* Entwicklung von Handlungsempfehlungen für eine mögliche Migration

### Umfang

Im Rahmen des PoC werden folgende Komponenten getestet:

|Einsatzzweck|Testkandidat|Testende
|------------|------------|--------
|[Dokumentbearbeitung](./documents/README.md)|[LibreOffice (LO)](https://www.libreoffice.org/download/download-libreoffice/)|48
|[E-Mail, Kalender, Kontakte, sowie Mailstack](./groupware/README.md) |[openDesk](https://opendesk.eu/) (OX Web-Client) |71
|[FAT-Client für E-Mail, Kalender, Kontakte](./groupware/README.md#vorstufe-test-thunderbird)|[Thunderbird (TB)](https://www.thunderbird.net/de/)|3
|[Schreibassistenzsystem](./languagetool/README.md)|[LanguageTool (LT)](https://languagetool.org/de)|48
|[Instant-Messaging Dienst](./messenger/README.md)|[Bundesmessenger (BuM)](https://gitlab.opencode.de/bwi/bundesmessenger)|95

Die Auswahl der Testkandidaten orientiert sich für den Bereich E-Mail, Kalender, Kontakte und Mailstack an den Entwicklungen von [openDesk](https://gitlab.opencode.de/bmi/opendesk/info), dem souveränen Arbeitsplatz des *[Zentrum Digitale Souveränität (ZenDis)](https://zendis.de)*.

Sowohl für E-Mail als auch Dokumentbearbeitung geht die Arbeitsgruppe zunächst noch von einem [FAT-Client](https://de.wikipedia.org/wiki/Fat_Client) Szenario aus. Gleichwohl nimmt die Bedeutung webbasierter Anwendungen zu.

### Ablauf
Die Clientsoftware zur Dokumentbearbeitung wird innerhalb der für E-Government zuständigen Abteilung des Thüringer Ministeriums für Digitales und Infrastruktur sowie einem Kernteam aus verschiedenen Ressorts schrittweise ausgerollt und den Mitarbeiter:innen so die Nutzung ermöglicht.
Zu Testzwecken wurden die Dienste [Schreibassistenzsystem](./languagetool/README.md) und [Instant-Messaging](./messenger/README.md) im Testbetrieb zentral zur Verfügung gestellt.
