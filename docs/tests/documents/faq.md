# Häufig gestellte Fragen

## Wie kann ich LibreOffice installieren?
Auf Deinem PC findest Du das sog. **Software Center**. Damit kannst Du LibreOffice direkt selbständig installieren. Ein Admin Zugang ist nicht erforderlich.

## Wie starte ich LibreOffice?
Du findest die LibreOffice Programme Writer, Calc, Impress usw. einfach über die Startleiste.

## Standardapp für `ODT`, `ODC`, `ODP` auswählen.
Damit Dateien im OpenDocument Format mit LibreOffice geöffnet werden, müssen die sog. Standard Apps für Dokumenttypen einmalig zugeordnet werden. Das kannst Du in den Systemeinstellungen unter **Standard Apps nach Dateityp auswählen** einstellen.

![Standard Apps nach Dateityp auswählen](files/extension-app-mapping.png)

## Sprachmodule
**Optionen - Spracheinstellungen - Linguistik**

Deaktiviere bitte die beiden Häkchen bei:

* `Hunspell-Rechtschreibprüfung`
* `Lightproof grammar checker (english)`

![LanguageTool Sprachmodule](files/language-modules.png)

> Diese Anpassungen werden künftig bei der Installation voreingestellt.

## Wie erstelle ich LibreOffice Dokumente?
Du kannst wie gewohnt direkt aus VIS heraus Dokumente erstellen. Entweder im VIS Vorgang rechte Maustaste "Neu aus Vorlage" oder per ++"Strg"+n++. Du findest die Vorlagen in der Kategorie "LibreOffice".
Für den Test wurden die gängigsten Standard-Vorlagen (z. B. Aktenvermerk, Leitungsvorlage usw.) mit LibreOffice umgesetzt.

![VIS Vorlagen Dialog mit LibreOffice Kategorie](files/vis-template-dialog-LO.png)

## Was soll ich testen?
Es geht darum herauszufinden, ob Du Deine täglichen Aufgaben im Zusammenhang mit Dokumenten auch mit LibreOffice umsetzen kannst. Deshalb fragen wir nicht nach bestimmten "Features".

## Wohin wende ich mich, wenn etwas (nicht) klappt?
Wende Dich bitte gern per Mail an <sebastian.schieke@tmdi.thueringen.de>
Die Anfragen werden gesammelt. Wir schauen, ob kurzfristig eine Lösung gefunden werden kann.

## Kann ich Word Dokumente auch mit LibreOffice bearbeiten?
Prinzipiell ist das möglich. Im Zuge des Tests wird LibreOffice jedoch *parallel* zur bestehenden Officesuite installiert. Das heißt, **`ODT`-Dokumente** werden **ausschließlich in LibreOffice** bearbeitet. **`DOCX` Dokumente** weiterhin **mit MS-Word**.

## Wird MS Office jetzt deinstalliert?
Nein. Für den Test werden beide Office Produkte parallel betrieben.

## Aber was soll das bringen?
Entscheidend für das Gelingen des Vorhabens ist, dass Daten aus der E-Akte direkt in Dokumente übernommen werden können. Das funktioniert in beiden Welten reibungslos. Jedoch wurde die benötigte Komponente für MS Office anderes implementiert als für LibreOffice.

## Können LibreOffice Dokumente auch zwischen Abteilungen ausgetauscht werden?
Wenn ein Dokument die eigene Organisationseinheit verlässt, sollte es ins PDF Format umgewandelt werden. Das geht in LibreOffice über das Menü _Datei → Exportieren als → als PDF exportieren ..._

![als PDF exportieren ...](files/lo-pdf-export.png)

## Kommentare erscheinen als "unbekannter Benutzer"
Das passiert, wenn keine Benutzerdaten in LibreOffice hinterlegt sind. Du kannst Deine Daten einfach über das Menü _Extras → Optionen_ im Bereich _LibreOffice → Benutzerdaten_ anpassen. Diese werden dann als Autor:in in Kommentaren angezeigt.
Künftig sollen diese Daten bereits während der Installation von LibreOffice voreingestellt werden. Siehe dazu auch [Issue](https://gitlab.opencode.de/thlv/agoss/-/issues/24)

![Benutzerdaten für LibreOffice anpassen](files/userdata.png)

## automatische Aktualisierung von VIS-Metadaten
Beim Erstellen eines Schriftstücks aus einer Vorlage werden die Metadaten aus der jeweiligen Akte/Dokument automatisch aus VIS eingefügt. Beim erneuten Öffnen ist eine Aktualisierung meist unnötig. Dies kann im Menü _VIS → VIS-Optionen_ eingestellt werden.

![Steuerung von Aktualisierungen über LibreOffice einstellen](files/vis-options.png)

## Kann ich die VIS-Metadaten manuell aktualisieren?
Ja. Im Menü _VIS → Alle VIS-Verknüpfungen aktualisieren_ werden alle Metadaten erneut aus VIS aktualisiert.
![VIS Verknüpfungen manuell aktualisieren](files/verknuepfungen-aktualisieren.png)

## Wie kann ich ein MS-Office Dokument ins LibreOffice Format umwandeln?
Du kannst jedes MS-Office Dokument unter dem Punkt "Speichern unter ..." auch im LibreOffice Format abspeichern.
Oder Du öffnest das Dokument mit LibreOffice und speicherst es dort im OpenDocument Format.

## Ich möchte viele Dokumente nach LibreOffice umwandeln. Wie gehe ich vor?
Im Lauf der Zeit sammeln sich oftmals viele Dokumente und Vorlagen an. Um sie möglichst schnell in ein offenes bzw. anderes Dokumentenformat zu übertragen, kann der [headless Modus von LibreOffice](https://help.libreoffice.org/24.2/de/text/shared/guide/start_parameters.html) benutzt werden.

## Gibt es auch ein [Menüband](https://de.wikipedia.org/wiki/Ribbon) in LibreOffice?
Die Benutzeroberfläche von LibreOffice kann sehr flexibel angepasst werden. Über das Menü _Ansicht → Benutzeroberfläche_ kannst Du z.B. die klassische Menüansicht durch ein Menüband ersetzen.
![Einstellungen zur Benutzeroberfläche](files/ribbon.png)

## Mir fehlt eine LibreOffice Dokumentvorlage im VIS, wurde die vergessen?
Im Rahmen des Tests wurden die gängigsten und aktuellsten Vorlagen für den Geschäftsbereich mit LibreOffice umgesetzt. Bitte [nimm Kontakt](./../../kontakt.md) uns auf. Wir können fehlende Vorlagen ergänzen und kurzfristig in VIS verfügbar machen.

## Ich habe Text aus einer MS Word Datei eingefügt, nun stimmt die Formatierung nicht mehr.
Wir empfehlen, Inhalte stets als "reinen Text" einzufügen. Oftmals wurde bereits in Quelldokumenten "von Hand" formatiert, was eine Übernahme der Formatierung in Absatzformatierungen erschwert. Die Tastenkombination in LibreOffice ist ++shift+ctrl+alt+v++

## Ich möchte Pivot Tabellen benutzen, wie gehe ich vor?
Pivot-Tabellen können in LibreOffice Calc erstellt und bearbeitet werden. Anleitungen finden sich in der [LibreOffice Hilfe](https://help.libreoffice.org/24.2/de/text/scalc/guide/datapilot.html) oder z.B. https://www.pivot-tabelle.de/openoffice-libreoffice/
