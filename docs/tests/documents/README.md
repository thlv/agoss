# Dokumentbearbeitung mit LibreOffice

Zu Testzwecken wird innerhalb der Abteilung für E-Government und IT des Thüringer Ministeriums für Digitales und Infrastruktur schrittweise LibreOffice ausgerollt. Darüber hinaus testen auch IT Referate einzelner Ressorts mit. Das geschieht [*parallel* zu den bisher eingesetzten Office](./faq.md#kann-ich-word-dokumente-auch-mit-libreoffice-bearbeiten) Produkten.

![LibreOffice Screenshot](files/lo-screenshot.png)

## Anforderungen Dokumentbearbeitung

* In der Thüringer Landesverwaltung wird zur digitalen Aktenhaltung VIS des Herstellers PDV eingesetzt. Dieses Fachverfahren **muss** also mit LibreOffice zusammenarbeiten.

* Schriftstücke sollten barrierefrei erstellt und bearbeitet werden können. Das betrifft sowohl die eigentliche Bearbeitung, als auch die Zugänglichkeit z.B. von PDF-Dokumenten. Dazu werden die Dokumente u.a. mit dem [PDF Accessibility Checker](https://pdfua.foundation/de/pac-2021-der-kostenlose-pdf-accessibility-checker/) der [PDF/UA](https://de.wikipedia.org/wiki/PDF/UA) Foundation überprüft.

## Installation und Konfiguration

Im Abschnitt [Installation und Konfiguration](./deployment.md) erfahren sie, wie LibreOffice in zentral verwalteten Umgebungen installiert und konfiguriert wird.

## Erfahrungswerte

Folgende Erfahrungswerte wurden bisher gesammelt und sollen hier transparent gemacht werden:

### Grundsätzliches

- aktuell wird mit LibreOffice Version 7.5.3.2 aus dem offiziellen Repository der Documentfoundation getestet.
- ein Upgrade auf Version 24.2.x ist in Vorbereitung
- Bestehende herstellerspezifische Office Dateien können mit LibreOffice weiter verarbeitet werden. Weiterführende Hinweise zum Funktionsvergleich von LibreOffice MS-Office finden sich auf der Website der [Documentfoundation](https://wiki.documentfoundation.org/Feature_Comparison:_LibreOffice_-_Microsoft_Office/de)
- Unter Umständen müssen herstellerspezifische Dokumentvorlagen in LibreOffice angepasst oder neu erstellt werden. Dies ist abhängig vom jeweiligen Anwendungsfall.

### LibreOffice und E-Akte-System der PDV
- Grundsätzlich lassen sich Dateien in beliebigen Formaten, so auch Dokumente im OpenDocument Format, einfach per Drag-and-Drop in der E-Akte ablegen. Dafür ist **keine zusätzliche Erweiterung** für LibreOffice notwendig.
- Sollen darüber hinaus Metadaten aus der E-Akte in einen Dokumententwurf einfließen, ist eine Erweiterung für LibreOffice notwendig.

### Barrierefreiheit und Zugänglichkeit

#### Barrierefreie Bedienung
LibreOffice bietet bereits eine Vielzahl von Unterstützungsmöglichkeiten, z.B. die vollständige Bedienbarkeit der gesamten Benutzeroberfläche über die Tastaturnavigation. Auf diese Weise haben Benutzer:innen die Möglichkeit, alle Eingaben über die Tastatur vorzunehmen, ohne eine Maus oder ein anderes Zeigegerät verwenden zu müssen.
Die integrierte Schnittstelle für assistierender Technologien (AT) zum Betriebssystem ermöglicht beispielsweise die Verwendung von Screenreader, Spracheingaben, Vergrößerungssoftware und Bildschirmtastaturen.
Weitere Unterstützungsmöglichkeiten sind auf der [Website der Document Foundation im Bereich Barrierefreiheit](https://de.libreoffice.org/get-help/accessibility/) dokumentiert.
Darüber hinaus wurden im Vergleich zur ursprünglich im Test eingesetzten Version LibreOffice 7.5.x in der Version 24.2.3.2 bereits zahlreiche [weitere Verbesserungen](https://wiki.documentfoundation.org/ReleaseNotes/24.2#Accessibility) bei der Bedienbarkeit vorgenommen. So kann bspw. die Prüfung der Barrierefreiheit kontinuierlich bereits während des Schreibens erfolgen. Dabei werden etwaige Barrieren bis auf Zeichenebene angezeigt und können direkt im Text lokalisiert werden. Teilweise werden auch Vorschläge zum direkten Beheben der jeweiligen Problemsituation aufgezeigt.

#### Barrierefreie Dokumente
Schriftstücke, die innerhalb und außerhalb der Thüringer Landesverwaltung erstellt, verarbeitet und geteilt werden, sollten grundsätzlich barrierefrei sein.
Für die Tests mit LibreOffice wurden in der Abteilung für E-Government gängige proprietäre Dokumentvorlagen im OpenDocument Format neu erstellt.

## Häufig gestellte Fragen (FAQ)
Häufig gestellte Fragen zu LibreOffice werden im [Bereich FAQ](./faq.md) beantwortet und gesammelt.
