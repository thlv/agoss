# Dokumentvorlagen mit Libreoffice

Dokumentvorlagen sind in er öffentlichen Verwaltung oftmals ein zentrales Elemenent im täglichen Arbeitsablauf. Die Anwendungsfälle variieren zum Teil stark. Meist entstehen Dokumentvorlagen an einer Stelle in einem Verwaltungsprozess. Sie werden initial erstellt und über lange Zeiträume weiter verwendet.

# Vorgehen

Prinzipiell können Dokumentvorlagen in das ODT Format umgewandelt oder neu gestaltet werden.

## neu Gestaltung von Vorlagen

Dieses Vorgehen bietet sich oftmals aus zwei Gründen an.

1. Um nicht mehr benötigte Vorlagen zu entfernen.
2. Um auf geänderte Rahmenbedingungen (z.B. Verbesserung der Zugänglichkeit, Integration einer neuen Fachanwendung) reagieren zu können.

## Umwandlung von Vorlagen

* Gute Ergebnisse lassen sich erzielen, wenn bestehende Vorlagen zunächst im .doc Format abgelegt werden
* die `DOC` Dateien können dann in LibreOffice geöffnet und im `ODT` Format gespeichert und weiter verarbeitet werden
* das Vorgehen kann durch [automatisierte Umwandlung](./faq.md#ich-mochte-viele-dokumente-nach-libreoffice-umwandeln-wie-gehe-ich-vor) beschleunigt werden

1. Korrekturen an der eigentlichen Vorlage
2. Attribute der Felder aus dem VIS in die Vorlage einfügen
3. Vorlagen in einer VIS-Vorlagenkategorie ablegen

## Erfahrungswerte im Zusammenhang mit der E-Akte

* Verknüpfungen zu Metadaten der E-Akte können, genau wie bei den proprietären Produkten, eingefügt werden
* Vorlagen müssen als `ODT` angelegt werden, da `OTT` (Templates) nicht aus VIS heraus geöffnet werden können
* Schutz abschalten, um ggf. schreibgeschützte Felder entfernen zu können. Siehe dazu in der [Onlinehilfe von LibreOffice](https://help.libreoffice.org/latest/de/text/swriter/guide/protection.html?DbPAR=WRITER#bm_id3150620) im Abschnitt Schutz abschalten.
