# Installation und Konfiguration

Für grundlegende Installationsvoraussetzungen und weiterführende Informationen wird der Abschnitt [Deployment und Migration aus dem Wiki der Documentfoundation](https://wiki.documentfoundation.org/Deployment_and_Migration) empfohlen.

## clientseitige Updates deaktivieren

Aktualisierungen der LibreOffice Suite erfolgen zentral gesteuert über entsprechendes Software Deployment. Aus diesem Grund werden clientseitige Updates deaktiviert. Seit Version 24.8.x verwendet LibreOffice für die Aktualisierungen den [Mozilla Update Service](https://discourse.mozilla.org/t/libreoffice-re-using-mozillas-mar-based-updating-functionality/125314). Dieser kann bereits während der Installation deaktiviert werden. Dazu wird dem MSI Installationspaket während der Installation der Parameter ``ISCHECKFORPRODUCTUPDATES=0`` mitgegeben werden.

## Schnellstart aktivieren

Über den Parameter ``QUICKSTART=1`` kann der Schnellstart von LibreOffice aktiviert werden. Dadurch wird LibreOffice bereits beim Systemstart in den Speicher geladen und Module wie der Writer starten dann schneller.

## zentrale Konfiguration von LibreOffice

- LibreOffice lässt sich zentral (z.B. durch das IT-Fachpersonal) konfigurieren. Das BSI hat dazu einen Leitfaden mit Beispielen veröffentlicht, die sich einfach nachnutzen lassen. <https://www.allianz-fuer-cybersicherheit.de/SharedDocs/Downloads/Webs/ACS/DE/BSI-CS/BSI-CS_146.html>. Dadurch können in großen, verteilten Umgebungen Einstellungen von LibreOffice, wie bspw. der zu verwendende Proxyserver oder der Umgang mit Makros systemweit voreingestellt werden.
- Im Rahmen des Tests wird die [zentrale Konfigurationsdatei](./files/TLRZ.xcd) während des Deployments ausgerollt.

## präferierte Anwendung zum Öffnen von ODF

- Die präferierte Anwendung zum Öffnen von Dateien im OpenDocument Format (\*.od\*) sollte per Reg-Key gesetzt werden.
- Zusätzlich sollte eingestellt werden, dass die Microsoft Office Produkte beim Starten nicht prüfen, ob sie die Standardanwendung für "alle Dokumente" sind. Dieses Verhalten lässt sich über Gruppenrichtlinien steuern:
    - [Excel](https://admx.help/?Category=Office2016&Policy=excel16.Office.Microsoft.Policies.Windows::L_ShowAlertIfNotDefault&Language=de-de)
    - [Word](https://admx.help/?Category=Office2016&Policy=word16.Office.Microsoft.Policies.Windows::L_AlertIfNotDefault&Language=de-de)
    - [Powerpoint](https://admx.help/?Category=Office2016&Policy=ppt16.Office.Microsoft.Policies.Windows::L_PromptIfPowerPointIsNotDefault&Language=de-de)

## VIS Integration

- Der VIS-Mandant sollte so konfiguriert werden, dass Dateien mit ODF-Extensions direkt via WebDAV an den Client übergeben und dort mit LibreOffice geöffnet werden können.
- Für das Erstellen von Dokumenten aus VIS heraus, müssen Dokumentvorlagen im OpenDocument Format in einer sog. VIS Vorlagenkategorie hinterlegt werden.
- Das *VIS-LibreOffice Add-in* für LibreOffice wird ausschließlich benötigt, wenn Metadaten aus VIS wie bspw. die Bearbeiterin eines Vorgangs, ein Aktenzeichen usw. in ein ODF-Dokument eingefügt werden sollen.
- Um das Add-in nutzen zu können, ist eine [Java Runtime Umgebung (JRE)](https://de.wikipedia.org/wiki/Java-Laufzeitumgebung) notwendig. Dafür wird das quelloffene [Eclipse Temurin](https://adoptium.net/de/temurin/releases/) verwendet, dass mit der [GPL-2.0](https://github.com/adoptium/jdk/blob/master/LICENSE) lizenziert ist.
- Das JRE muss auf dem Client **vor** der Installation von LibreOffice installiert werden.
- Die VIS Metadaten werden als [Felder](https://help.libreoffice.org/latest/de/text/swriter/guide/fields.html) in ein LibreOffice Dokument eingefügt.

## Steuerung über Gruppenrichtlinien (GPO)

GPO können genutzt werden, um z.B. Benutzerinformationen von einem Identity Provider zu übernehmen. Die passenden ADMX GPO Vorlagen sind im Wiki der Dokumentfoundation im Bereich [Windows_Group_Policy_ADMX_files](https://wiki.documentfoundation.org/Deployment_and_Migration#Windows_Group_Policy_ADMX_files) zu finden.
