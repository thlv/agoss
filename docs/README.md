# Aktuelle Lage in der Thüringer Landesverwaltung (THLV)

- Das Thüringer E-Government-Gesetz (ThürEGovG) [§ 4 - Offene Standards und Freie Software](https://landesrecht.thueringen.de/bsth/document/jlr-EGovGTHpP4) und das Thüringer Vergabegesetz (ThürVgG) [§ 4, Absatz 2](https://landesrecht.thueringen.de/bsth/document/jlr-VergabeGTH2019V2P4) nehmen den Einsatz quelloffener Software in der Landesverwaltung verstärkt in den Fokus.
- In der Landesverwaltung sind ca. 20.000 Arbeitsplatz-PCs im Einsatz. Ausgestattet in der Regel mit Microsoft Windows als Betriebssystem und Microsoft Office für die Bürokommunikation.
- Die Softwareanbieter drängen die Nutzer zunehmend in die Cloud, um ihr eigenes Geschäftsmodell auszubauen
- Abhängigkeiten bestehen in der THLV hauptsächlich zu Herstellern wie Microsoft (Arbeitsplatzsoftware), Oracle (Datenbanken / Fachapplikationen, die Oracle als Basis nutzen) und PDV.
- Open-Source-Software hat bisher in der Verwaltung den nötigen Stellenwert erreicht. Teilweise erfolgt der Einsatz unbewusst oder weil die Software zunächst kostenlos und ohne einschränkende Lizenzbedingungen genutzt werden kann. Erfreulich ist, dass diesbezüglich ein Umdenken begonnen hat, und Open-Source-Software zunehmend als "Enterprise-Ready" angesehen wird und die Verwendung weiter stark ansteigt.
- Die Vorteile des Einsatzes von Open-Source-Software sind kein reines Erkenntnisthema mehr. Siehe dazu auch die Absichtserklärung zur Erarbeitung eines [souveränen Arbeitsplatzes](https://finanzen.thueringen.de/aktuelles/medieninfo/detailseite/thueringen-unterzeichnet-gemeinsame-absichtser-klaerung-der-cios-der-laender-und-des-bundes-zur-staerkung-der-digitalen-souveraenitaet-ziel-ist-auch-souveraener-arbeitsplatz-fuer-verwaltung) in 2021. Um diese Vorteile nutzen zu können, bedarf es der verstärkten Einführung und Nutzung von Open-Source-Software.
- Deshalb wurde im Herbst 2022 eine Ressort übergreifende Arbeitsgemeinschaft (AGOSS) gebildet, die konkrete [Einsatzszenarien von Open-Source basierter Office-Software innerhalb der Thüringer Landesverwaltung](./tests/README.md) prüft.

# Ziele

- Reduktion von Abhängigkeiten von Geschäfts- und Lizenzmodellen der großen Softwarehersteller (z.B. mögliche Zwangsnutzung von Cloud-Diensten oder Lock-in-Effekte)
- Innovation ermöglichen
    - Schaffung von Alternativen usw.
    - Stärkung der Verhandlungspositionen ggü. Microsoft, Oracle und Co.
    - Erschließung neuer Handlungsspielräume durch Einsatz interoperabler, offener Dienste und Infrastrukturen
- Förderung des Marktes, um dort einen echten Wettbewerb zu erreichen, wo derzeit noch Monopolstellungen vorherrschen
- Modernisierung der Verwaltung vorantreiben und Bedienstete motivieren, neue Software einzusetzen
- dokumentierte und [offene Schnittstellen](https://en.wikipedia.org/wiki/Open_API) und Standards (z.B. bei Dateiformaten) etablieren
- Bündelung der Vorhaben durch Integration und Verzahnung bestehender Einzellösungen
- Nachhaltigkeit durch die Wiederverwendbarkeit von Software stärken

# Test Open-Source Office-Software

* [Einsatzszenarien von Open-Source basierter Office-Software innerhalb der Thüringer Landesverwaltung](./tests/README.md)

# weiterführende Literatur

Weiterführende Literatur zu Open-Source-Software im Verwaltungskontext sammeln wir im [Literaturverzeichnis](./literatur.md).
